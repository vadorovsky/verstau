# verstau

Symlink manager leveraging TOML configuration files. Written with the following
use cases in mind:

* Managing configuration files ("dotfiles" git repositories).
* Managing multiple versions of manually installed software (e.g. inside
  `/usr/local`).

## Configuration

Verstau looks for `verstau.toml` file in the given directory, which has the
following syntax:

```toml
[my_file_1]
source = "my_file_1"
destination = "~/my_file"

[my_file_2]
source = "my_file_2"
destination = "/etc/my_file_2"
```

## Usage

To apply the configuration, launch the following command (from the directory
containing `verstau.toml`):

```shell
verstau apply
```

If the command fails, because a destination file already exists, **and you are
sure that you want to replace it**, you can use `--force` option to overwrite
existing files:

```shell
verstau apply --force
```

Or `--skip-existing` to add only new symlinks:

```shell
verstau apply --skip-existing
```

## Example

[This repository](https://gitlab.com/vadorovsky/dotfiles) uses verstau to
manage configuration files, both system-wide (/etc) and user-wide.
