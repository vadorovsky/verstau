use std::{env, fs, os::unix::fs::symlink, path::PathBuf};

use anyhow::{Context, Result};
use clap::{Parser, Subcommand};
use glob::glob;
use toml_edit::{Document, Item};

#[derive(Parser)]
#[command(author, version, about)]
struct Args {
    #[clap(subcommand)]
    subcommand: SubCommand,
}

#[derive(Subcommand)]
enum SubCommand {
    /// Apply symlinks.
    Apply {
        /// The path to the configuration file.
        #[clap(short, long)]
        config: Option<PathBuf>,
        /// Force overwrite of existing files.
        #[clap(short, long)]
        force: bool,
        /// Skip already existing symlinks.
        #[clap(short, long)]
        skip_existing: bool,
    },
}

fn main() -> Result<()> {
    let args = Args::parse();

    match args.subcommand {
        SubCommand::Apply {
            config,
            force,
            skip_existing,
        } => apply(config, force, skip_existing),
    }
}

fn read_config(config: Option<PathBuf>) -> Result<Document> {
    let config = match config {
        Some(config) => config,
        None => env::current_dir()
            .context("Could not find the current working directory")?
            .join("verstau.toml"),
    };

    if !config.exists() {
        anyhow::bail!("Could not find the configuration file at {:?}", config);
    }

    let config: Document = std::fs::read_to_string(config)
        .context("Could not read the configuration file")?
        .parse()
        .context("Could not parse the configuration file")?;

    Ok(config)
}

fn apply(config: Option<PathBuf>, force: bool, skip_existing: bool) -> Result<()> {
    let config = read_config(config)?;

    for (key, item) in config.iter() {
        if let Item::Table(table) = item {
            let source = table
                .get("source")
                .context("Could not find the source path")?
                .as_str()
                .context("Could not parse the source path")?;
            for entry in glob(&source)? {
                match entry {
                    Ok(entry) => {
                        let entry = fs::canonicalize(entry)?;
                        let destination = table
                            .get("destination")
                            .context("Could not find the target path")?
                            .as_str()
                            .context("Could not parse the target path")?;
                        let destination =
                            PathBuf::from(shellexpand::tilde(destination).to_string());
                        let destination = if !entry.ends_with(&source) {
                            destination.join(
                                entry
                                    .file_name()
                                    .ok_or(anyhow::anyhow!("Entry has no file name"))?,
                            )
                        } else {
                            destination
                        };
                        apply_file(key, entry, destination, force, skip_existing)?;
                    }
                    Err(e) => eprintln!("Failed to process the entry: {}", e),
                }
            }
        }
    }

    Ok(())
}

fn apply_file(
    key: &str,
    source: PathBuf,
    destination: PathBuf,
    force: bool,
    skip_existing: bool,
) -> Result<()> {
    if let Some(parent) = destination.parent() {
        if !parent.exists() {
            fs::create_dir_all(&parent).context("Failed to create parent directory")?;
        }
    }

    if let Ok(metadata) = fs::symlink_metadata(&destination) {
        if metadata.file_type().is_symlink() || metadata.file_type().is_file() {
            if skip_existing {
                println!(
                    "Skipping already existing item {}, {:?} to {:?}",
                    key, source, destination
                );
                return Ok(());
            }
            if force {
                fs::remove_file(&destination).context("Could not remove the existing file")?;
            } else {
                anyhow::bail!(
                    "The destination path {:?} already exists. \
                                    Use --force to overwrite it.",
                    destination
                );
            }
        }
    }

    println!("Linking item {}, {:?} to {:?}", key, source, destination);
    symlink(source, destination).context("Could not create the symbolic link")?;

    Ok(())
}
